# Jose Gustavo, Ricardo, Lauro

import random

def preenche_vetor():

    vetor = []

    #preenchendo o vetor com 30 numeros aleatorios entre 1 e 30
    for _ in range(0,30):
        vetor.append(random.randint(1,30))

    return vetor

def imprime_vetor(vetor):

    print(vetor)

def ordena_vetor(vetor):

    tam = len(vetor)

    #selection sort
    for i in range(0, tam - 1):
        maior = i
        for j in range(i+1, tam):
            if vetor[j] > vetor[maior]:
                maior = j
        if i != maior:
            aux = vetor[i]
            vetor[i] = vetor[maior]
            vetor[maior] = aux

if __name__ == "__main__":
    
    vetor = preenche_vetor()
    #imprimindo o vetor antes da ordenação
    imprime_vetor(vetor)
    ordena_vetor(vetor)
    #imprindo depois da ordenação
    imprime_vetor(vetor)

