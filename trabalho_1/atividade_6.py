# Lauro, Jose Gustavo, Ricardo

def preenche_vetor():
    # vetor = [3,4,5,6,7,8,9,11,2,41,241,2,213,4,2,4]
    vetor = []
    n = 0
    while n < 20:
        numero = int(input())
        if numero > 1:
            vetor.append(numero)
            n += 1

    return vetor


#função para verificar se um número é composto    
def numero_composto(numero):
    for i in range(2, numero):
        if numero % i == 0:
            return 1
            #um numero é composto quando não é primo. Então fiz um teste de primalidade.
            #se um numero N tem um divisor entre 2 e N-1, então ele não é primo, logo é composto

    #se um numero é primo, não é composto. retorno ZERO
    return 0

def organiza_vetor(vetor, vetor_auxiliar = [], indice_composto = 0, indice_nao_composto = 1, indice_vetor = 0):
    #se a leitura chegou ao final do vetor, retornar vetor organizado.
    if indice_vetor == len(vetor):
        return vetor_auxiliar
    #se o número na posição atual é composto, inserir número na posição do indice_composto
    if numero_composto(vetor[indice_vetor]):
        vetor_auxiliar.insert(indice_composto, vetor[indice_vetor])
        indice_composto += 1
        #incrementar índice composto, já que foi utilizado.
    else:
        vetor_auxiliar.insert(len(vetor) - indice_nao_composto, vetor[indice_vetor])
        indice_nao_composto += 1
        #incrementar índice nao_composto, já que foi utilizado.

    #incrementar índice vetor, nova posição.
    indice_vetor += 1
    return organiza_vetor(vetor, vetor_auxiliar, indice_composto, indice_nao_composto, indice_vetor)
 

if __name__ == "__main__":
    vetor = preenche_vetor()
    print(vetor)
    print('-' * 100)
    print(organiza_vetor(vetor))