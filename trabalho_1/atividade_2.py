# Ricardo, José Gustavo, Lauro

import random

def gera_matriz():

    matriz = []
    # exemplo de matriz ortogonal
    # matriz = [[-1/2,1/2,1/2,1/2],[1/2,-1/2,1/2,1/2],[1/2,1/2,-1/2,1/2],[1/2,1/2,1/2,-1/2]]

    #preenchendo a matriz 4x4 com numeros aleatorios de 0 a 29
    for _ in range(0, 4):
        linha = [random.randint(0,29) for _ in range(0, 4)]
        matriz.append(linha)

    return matriz

def matriz_transposta(matriz):

    matriz_transposta = []

    #formando a matriz transposta
    for coluna in range(0, 4):
       row = [matriz[linha][coluna] for linha in range(0, 4)]
       matriz_transposta.append(row)

    return matriz_transposta

def multiplica_matrizes(matriz, m_transposta):

    #resultado da multiplicação das duas matrizes
    m_mult = []

    for _ in range(0, 4):
        row = [0 for _ in range(0, 4)]
        m_mult.append(row)


    for i in range(0, 4):
        for j in range(0, 4):
            m_mult[i][j] = 0
            for k in range(0, 4):
                m_mult[i][j] += matriz[i][k] * m_transposta[k][j]

    return m_mult

def verifica_ortogonal(m_multi):

    matriz_identidade = [[1,0,0,0],[0,1,0,0],[0,0,1,0],[0,0,0,1]]

    #comparar o resultado da multiplicações entre as matrizes com a matriz identidade
    for i in range(0,4):
        for j in range(0,4):
            if(matriz_identidade[i][j] != m_multi[i][j]):
                return 0
    
    return 1
        
if __name__ == "__main__":
    
    matriz = gera_matriz()

    m_transposta = matriz_transposta(matriz)
    matrizes_multiplicadas = multiplica_matrizes(matriz, m_transposta)
    
    if(verifica_ortogonal(matrizes_multiplicadas)):
        print("Matriz ortogonal")
    else:
        print("Matriz não ortogonal")

