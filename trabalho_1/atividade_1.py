# Ricardo, Lauro, José Gustavo

import random
import functools

def generate_matrix(n, m):
  matrix = []
  for _ in range(0, n):
    row = [random.randint(1, 20) for _ in range(0, m)]
    matrix.append(row)
  return matrix

def verify_matrix(matrix, n, m):
  response_sum_min = None
  response_prod_max = None

  for j in range(0, m):
    selected_values = [row[j] for row in matrix]
    sum_value = functools.reduce(lambda a, b: a + abs(b), selected_values)
    if response_sum_min is None or response_sum_min >= sum_value:
      response_sum_min = sum_value
  
  for i in range(0, n):
    prod_value = functools.reduce(lambda a, b: a * b, matrix[i])
    if response_prod_max is None or response_prod_max <= prod_value:
      response_prod_max = prod_value

  return response_sum_min <= response_prod_max

if __name__ == "__main__":
  matrix = generate_matrix(4, 5)
  if verify_matrix(matrix, 4, 5):
    print('Condicao Satisfeita')
  else:
    print('Condicao Nao Satisfeita')
