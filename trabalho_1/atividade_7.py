# Jose Gustavo, Ricardo, Lauro

import random

def generate_list():
  return [random.randint(1, 100) for _ in range(0, 50)]

def list_max_value(value_list):
  if len(value_list) > 0:
    x = value_list.pop()
    y = list_max_value(value_list)
    if x > y:
      return x
    else:
      return y
  else:
    return 0

if __name__ == '__main__':
  value_list = generate_list()
  print("O maior valor é: ", list_max_value(value_list))
