# Lauro, Jose Gustavo, Ricardo

def calculate_power(a, b):
  if b == 0:
    return 1
  else:
    return a * calculate_power(a, b - 1)

if __name__ == '__main__':
  base_value = int(input('Digite a base: '))
  if base_value == 0:
    raise ValueError('O valor da base é inválido')
  power_value = int(input('Digite o expoente: '))
  if power_value < 0:
    raise ValueError('O valor do expoente é inválido')
  result = calculate_power(base_value, power_value)
  print('Resultado: ', result)
