# Lauro, Ricardo, Jose Gustavo

def preenche_vetor():
    vetor = []

    for _ in range(0,20):
        vetor.append(int(input()))

    return vetor

def organiza_vetor(vetor):

    par = 0
    impar = 1
 
    for i in range(0, 20, 2):
        if(vetor[i] % 2 != 0):
            for j in range(impar, 20, 2):
                if(vetor[j] % 2 == 0):
                    par = j
                    impar += 2
                    break
            
            aux = vetor[i]
            vetor[i] = vetor[par]
            vetor[par] = aux
  
if __name__ == "__main__":

    vetor = preenche_vetor()

    organiza_vetor(vetor)

    print(vetor)
